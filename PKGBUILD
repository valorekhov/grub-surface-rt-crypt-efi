pkgname="grub-surface-rt"
pkgver=2.06.r32.g261cb5bdc
pkgrel=1
pkgdesc="GNU GRand Unified Bootloader (2) with patches applied for supporting booting on Surface RT, Surface RT 2"
arch=('armv7h')
url="https://www.gnu.org/software/grub/"
license=('GPL3')
depends=('device-mapper' 'freetype2' 'fuse2' 'gettext')
makedepends=('autogen' 'bdf-unifont' 'git' 'help2man'
             'python' 'rsync' 'texinfo' 'ttf-dejavu')

if [[ "${_grub_emu_build}" == "1" ]]; then
    depends+=('sdl')
    makedepends+=('libusb')
    optdepends+=('libusb: For grub-emu USB support')
fi

provides=("grub")
conflicts=("grub")
backup=('etc/default/grub'
        'etc/grub.d/40_custom')
install="${pkgname}.install"
source=("grub::git+https://git.savannah.gnu.org/git/grub.git"
        "grub-extras::git+https://git.savannah.gnu.org/git/grub-extras.git"
        "gnulib::git+https://git.savannah.gnu.org/git/gnulib.git"
        'detect-archlinux-initramfs.patch'
        '0000-SurfaceRT-Vid-Handle.patch'
        '0001-Cryptomount-support-LUKS-detached-header.patch'
        '0002-Cryptomount-support-key-files.patch'
        #'0003-Cryptomount-luks-allow-multiple-passphrase-attempts.patch'
        #'0004-Cryptomount-support-plain-dm-crypt.patch'
        #'0005-Cryptomount-support-for-hyphens-in-UUID.patch'
        #'0006-Retain-constness-of-parameters.patch'
        #'0007-Add-support-for-using-a-whole-device-as-a-keyfile.patch'
        'grub.default'
        'grub-mkimage-surface-rt.sh')
sha256sums=('SKIP'
            'SKIP'
            'SKIP'
            '580a81b00088773d554832b0d74c85bf16fec37728802973c45993bcb97cd7d5'
            '686c971c95539c15757c5886e2b770ea399448b2a661ef0d4cba4802a282d390'
            'a1369e08206e3239e7f5d4f21365415caa884bcea050a3ca111c4bb6b1c5846f'
            '9638520f020f4f131d46b9d048d184756a07202427d14fb91b5dec97975452e2'
            '791fadf182edf8d5bee4b45c008b08adce9689a9624971136527891a8f67d206'
            'ee77dbc45c32b884bfa484fddc8ad0f53b66e19d76cac8165f406ab82a2642dc')
 
prepare() {
    cd grub

    local src
    for src in "${source[@]}"; do
        src="${src%%::*}"
        src="${src##*/}"
        [[ $src = *.patch ]] || continue
        echo "Applying patch $src..."
        patch -Np1 < "../$src"
    done

    # Fix DejaVuSans.ttf location so that grub-mkfont can create *.pf2 files for starfield theme.
    sed 's|/usr/share/fonts/dejavu|/usr/share/fonts/dejavu /usr/share/fonts/TTF|g' -i "configure.ac"

    # Modify grub-mkconfig behaviour to silence warnings FS#36275
    sed 's| ro | rw |g' -i "util/grub.d/10_linux.in"

    # Modify grub-mkconfig behaviour so automatically generated entries read 'Arch Linux' FS#33393
    sed 's|GNU/Linux|Linux|' -i "util/grub.d/10_linux.in"

    # Pull in latest language files
    ./linguas.sh

    # Remove lua module from grub-extras as it is incompatible with changes to grub_file_open   
    # http://git.savannah.gnu.org/cgit/grub.git/commit/?id=ca0a4f689a02c2c5a5e385f874aaaa38e151564e
    rm -rf "$srcdir"/grub-extras/lua
}

pkgver() {
    cd grub
    git describe --long --tags | sed 's/^grub.//;s/\([^-]*-g\)/r\1/;s/-/./g'
}

build() {
    cd grub
    export GRUB_CONTRIB="$srcdir"/grub-extras
    export GNULIB_SRCDIR="$srcdir"/gnulib

    CFLAGS="-O2 -pipe -fno-plt \
        -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security \
        -fstack-clash-protection"

    echo "Effective CFLAGS: $CFLAGS"

    ./bootstrap 

    _arch="arm-efi"
    mkdir -p "$srcdir"/grub/build_"$_arch"
    cd "$srcdir"/grub/build_"$_arch"

    # * _FORTIFY_SOURCE requires compiling with optimization warnings
    #   become errors due to a -Werror added during ./configure tests.
    #   This results in an incorrect configuration and only by adding -O2
    #   to CPPFLAGS does this problem seem to be worked around.
    ../configure --with-platform=efi \
            --target=arm-linux-gnueabihf  \
            --prefix="/usr" \
            --sbindir="/usr/bin" \
            --sysconfdir="/etc" \
            --enable-boot-time \
            --enable-cache-stats \
            --enable-device-mapper \
            --enable-grub-mkfont \
            --enable-grub-mount \
            --enable-mm-debug \
            --enable-nls \
            --disable-silent-rules \
            --disable-werror \
            CPPFLAGS="$CPPFLAGS -O2" 

    make -j $(nproc)
}

package() {
    cd grub

    _arch="arm-efi"
    cd "$srcdir"/grub/build_"$_arch"
    make DESTDIR="$pkgdir" bashcompletiondir=/usr/share/bash-completion/completions install

    # Install /etc/default/grub (used by grub-mkconfig)
    install -D -m0644 "$srcdir"/grub.default "$pkgdir"/etc/default/grub

    install -D -m0644 "$srcdir"/grub-mkimage-surface-rt.sh "$pkgdir"/etc/default/grub-mkimage-surface-rt.sh.default
    
    # Tidy up
    find "$pkgdir"/usr/lib/grub \( -name '*.module' -o \
                                   -name '*.image' -o \
                                   -name 'kernel.exec' -o \
                                   -name 'gdb_grub' -o \
                                   -name 'gmodule.pl' \) -delete
}
