OUT_FILE="${1:-bootarm.efi}"

grub-mkimage -O arm-efi -d . -o "$OUT_FILE" -p / luks2 cryptodisk gcry_rijndael part_gpt pbkdf2 gcry_sha512 gcry_sha256 part_msdos \
        fat ext2 normal chain boot configfile linux gfxterm videoinfo efi_gop all_video video video_fb loadenv help reboot raid6rec raid5rec \
        mdraid1x mdraid09 lvm diskfilter zfsinfo zfscrypt gcry_rijndael gcry_sha1 zfs true test sleep search search_fs_uuid search_fs_file \
        search_label png password_pbkdf2 gcry_sha512 pbkdf2 part_apple minicmd memdisk lsacpi lssal lsefisystab lsefimmap lsefi disk keystatus jpeg \
        iso9660 halt gfxterm_background gfxmenu trig bitmap_scale video_colors bitmap font fshelp efifwsetup echo terminal gettext efinet net \
        priority_queue datetime bufio cat btrfs gzio lzopio crypto acpi extcmd mmap